// Jose Badilla -- Lab 5 -- main.cpp //

#include <iostream>
#include <fstream>
#include <string>
#include "Puzzle.h"

using namespace std;

int main(void) {

	int userRow, userCol, userInput;
	string filename;	//file name for sudoku file
	string wordFilename;//file name for wordoku file
	cout << "WORDOKU" << endl << "=======" << endl;
	cout << "Enter Wordoku file name: ";
	cin >> wordFilename;
	Puzzle <char> myPuzzle(wordFilename);	//creates templated instance for wordoku
	cout << myPuzzle << endl;	//displays wordoku

	cout << "SUDOKU GAME" << endl << "===========" << endl;	//separates wordoku display and sudoku game
	cout << "NOTE: Board values of 0 constitute empty spaces" << endl;
	cout << "Enter Sudoku file name: ";
	cin >> filename;
	Puzzle <int> mySudokuPuzzle(filename);	//creates templated instance of integer board
	cout << mySudokuPuzzle << endl;

	while (mySudokuPuzzle.isSolved() == 0) {//while loop determines when the game is over
		cout << "Please choose a row: " << endl;
		cin >> userRow;
		while (userRow < 1 | userRow > 9) {	//avoid erroneous input
			cout << "Invalid input. Please choose a valid row number: ";
			cin >> userRow;
		}
		cout << "Please choose a column: " << endl;
		cin >> userCol;
		while (userCol < 1 | userCol > 9) {	//avoid erroneous input
			cout << "Invalid input. Please choose a valid column number: ";
			cin >> userRow;
		}
		cout << "Please enter the number you would like to place in ROW " << userRow << " COLUMN " << userCol << ": "; 
		cin >> userInput;
		while (userInput < 1 | userInput > 9) {	//avoid erroneous input
			cout << "Invalid input. Please choose a number from 1-9: ";
			cin >> userInput;
		}
		if (mySudokuPuzzle.checkInput(userRow, userCol, userInput)) {
			cout << "Input valid!" << endl;
			cout << mySudokuPuzzle << endl;	//use of overloaded operator
		}
		else {
			cout << "Incorrect placement! Please try again." << endl;	//placement is not valid
		}
	}
	cout << "You solved the Sudoku puzzle!";
	return 0;
}