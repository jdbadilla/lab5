// Jose Badilla -- Lab 5 -- Puzzle.h //

#ifndef PUZZLE_H
#define PUZZLE_H

#include <iostream>
#include <fstream>
#include <vector>
#include <ctype.h>
#include <cmath>
#include <sstream>
using namespace std;

template <typename T> class Puzzle;
template <typename T> ostream &operator<<(ostream &, Puzzle<T> &);

template <typename T>
class Puzzle {

	friend ostream &operator<< <> (ostream &, Puzzle<T> &);

	public:
		Puzzle(string);	//default constructor
		~Puzzle();	//default deconstructor
		int isSolved();
		int checkInput(int, int, int);
		int checkRow(int, int);
		int checkCol(int, int);
		int checkSquare(int, int, int);

	private:
		vector< vector<T> > Sudoku;	//Creates 2D vector

};
#endif

template <typename T>
Puzzle<T>::Puzzle(string str) {		//constructor
	ifstream inFile(str.c_str());
	int i, j;

	if (inFile.is_open()) {
		for ( i = 0; i < 9; i++ ) {
			Sudoku.push_back(vector<T>(9));
			for ( j = 0; j < 9; j++ ) {
				T Template;
				inFile >> Template;
				Sudoku[i].push_back(j);
				Sudoku[i].at(j) = Template;		//assigns respective text character to cell in puzzle
			}
		}
		inFile.close();
	}
}

template <typename T>
Puzzle<T>::~Puzzle() {

}

template <typename T>
ostream &operator<<(ostream &output, Puzzle<T> &Puzzle) {	//overloaded operator displays puzzle
	output << endl << "Puzzle:" << endl << endl;
	for (int i = 0; i < 9; i++) {
		for (int j = 0; j < 9; j++) {
			output << Puzzle.Sudoku[i][j] << " ";	//prints a space between analogous numbers
			if (j == 8){
				output << endl;	//prints a new line character at the end of row
			}
		}
	}
	return output;
}

template <typename T>
int Puzzle<T>::checkInput(int row, int column, int input) {
	if (checkRow(row, input) == 1 && checkCol(column, input) == 1 && checkSquare(row, column, input) == 1){
		Sudoku[row - 1][column - 1] = input;	//subtracts by 1 to account for integer allocated in Sudoku
		return 1;
	}
}

template <typename T>
int Puzzle<T>::checkRow(int row, int input) {
	for (int ic = 0; ic < 9; ic++) {		//checks columns of specified row to check if value can be placed
		if (input == Sudoku[row - 1][ic]) {	//[row - 1] accounts for the integer allocated in Sudoku[0]
			return 0;
		}
	}
	return 1;
}

template <typename T>
int Puzzle<T>::checkCol(int column, int input) {
	for (int ir = 0; ir < 9; ir++) {		//checks rows of specified column to check if value can be placed
		if (input == Sudoku[ir][column - 1]) { //[column - 1] accounts for the integer allocated in Sudoku[0]
			return 0;
		}
	}
	return 1;
}

template <typename T>
int Puzzle<T>::checkSquare(int row, int column, int input) {
	int i, j;
	int subN = sqrt(Sudoku.size());	 // #(columns and rows in sub-squares on board) = sqrt[#(columns and rows in puzzle)]
	int subRow = row - (row % subN); //set adjust values to the top left of the sub-square to search down the 3 subcolumns and 3 subrows
	int subCol = column - (column % subN);
	for (i = subRow; i < subRow+subN; i++) {	//search sub-square
		for (j = subCol; j < subCol; j++) {
			if (input == Sudoku[i][j]) {
				return 0;
			}
		}
	}
	return 1;
}

template <typename T>
int Puzzle<T>::isSolved() {		//determines if puzzle is solved
	int i, j;
	for (i = 0; i < Sudoku.size(); i++) {
		for (j = 0; j < Sudoku.size(); j++) {
			if (Sudoku[i][j] == 0) {	//as long as there is still a cell equal to 0, isSolved will return false (0)
				return 0;
			}
		}
	}
	return 1;
}